import { createRouter, createWebHistory } from 'vue-router'
import Index from "../page/Index.vue";
import Chat from "../page/Chat.vue";
const routes = [
    { path: '/', component: Chat },
    { path: '/index', component: Index },
    // 其他路由配置
]
const router = createRouter({
    history: createWebHistory(),
    routes,
})
export default router

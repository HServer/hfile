package net.hserver.file.bean;

import cn.hutool.json.JSONUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Msg {
    //目标接收者
    private String dst;
    private Type type;
    private Object payload;

    public enum Type {
        OPEN,HEARTBEAT,OFFER,ANSWER,CANDIDATE
    }

    public Msg(Type type) {
        this.type = type;
    }

    public Msg(Type type, Object payload) {
        this.type = type;
        this.payload = payload;
    }

    public String getJson() {
        return JSONUtil.toJsonStr(this);
    }
}

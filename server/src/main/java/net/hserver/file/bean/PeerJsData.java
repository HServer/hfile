package net.hserver.file.bean;

import cn.hserver.plugin.web.handlers.Ws;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PeerJsData {
    private String id;
    private String key;
    private String token;
    private String version;

    private Ws ws;

}

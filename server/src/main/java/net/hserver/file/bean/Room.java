package net.hserver.file.bean;

import cn.hserver.plugin.web.handlers.Ws;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Room {
    private String roomId = UUID.randomUUID().toString();
    private Ws to;
    private Ws from;

    public void sendGroupSuccess() {
        //发送群聊消息
    }
}

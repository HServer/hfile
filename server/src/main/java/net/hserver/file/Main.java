package net.hserver.file;

import cn.hserver.HServerApplication;
import cn.hserver.core.ioc.annotation.HServerBoot;

@HServerBoot
public class Main {
    public static void main(String[] args) {
        HServerApplication.run(Main.class,80);
    }
}

package net.hserver.file.controller;

import cn.hserver.plugin.web.annotation.Controller;
import cn.hserver.plugin.web.annotation.GET;
import cn.hserver.plugin.web.annotation.WebSocket;
import cn.hserver.plugin.web.handlers.Ws;
import cn.hserver.plugin.web.interfaces.WebSocketHandler;
import cn.hutool.core.lang.UUID;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import net.hserver.file.bean.Msg;
import net.hserver.file.bean.PeerJsData;

import java.rmi.server.UID;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static net.hserver.file.bean.Msg.Type.OPEN;
@Slf4j
@Controller("/peerjs/")
public class PeerjsController {

    @GET("id")
    public String id(){
        return UUID.randomUUID().toString();
    }

}
